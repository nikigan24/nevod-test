<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $photo = file_get_contents('https://picsum.photos/1400/600');
        $name = Str::random(20). '.png';
        file_put_contents('public/image/' . $name, $photo);
        Image::make($photo)->resize(700, 300)->save('public/image/resized/' . $name);
        $content = $this->faker->realText();
        return [
            'title' => $this->faker->realText(20),
            'preview' => asset('image/resized/' . $name),
            'image' => asset('image/' . $name),
            'excerpt' => Str::substr($content, 0, 50),
            'content' => $content
        ];
    }
}
