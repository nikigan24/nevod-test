<?php

namespace Database\Seeders;

use App\Models\OptionValue;
use App\Models\Product;
use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{

    /**
     * The current Faker instance.
     *
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Create a new seeder instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->faker = $this->withFaker();
    }

    /**
     * Get a new Faker instance.
     *
     * @return \Faker\Generator
     */
    protected function withFaker()
    {
        return Container::getInstance()->make(Generator::class);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = Product::query()->create([
            'name' => 'Пепперони',
            'price' => 395,
            'image' => asset('image/products/thumb_909_products_medium.webp'),
            'proteins' => 7.9,
            'fats' => 12.3,
            'carbohydrates' => 19.6,
            'weight' => 550,
            'category_id' => 1
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 1,
            'value' => 395
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 2,
            'value' => 645
        ]);

        $product = Product::query()->create([
            'name' => 'Маргарита',
            'price' => 395,
            'image' => asset('image/products/thumb_912_products_medium.webp'),
            'proteins' => 7.9,
            'fats' => 12.3,
            'weight' => 550,
            'carbohydrates' => 19.6,
            'category_id' => 1
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 1,
            'value' => 395
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 2,
            'value' => 645
        ]);

        $product = Product::query()->create([
            'name' => 'Гавайская',
            'price' => 395,
            'image' => asset('image/products/thumb_910_products_medium.webp'),
            'proteins' => 7.9,
            'fats' => 12.3,
            'weight' => 550,
            'carbohydrates' => 19.6,
            'category_id' => 1
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 1,
            'value' => 395
        ]);

        OptionValue::query()->create([
            'product_id' => $product->id,
            'option_id' => 2,
            'value' => 645
        ]);

        Product::query()->create([
            'name' => 'Ёбоши',
            'price' => 969,
            'image' => asset('image/products/fb6e9af1-1b74-4687-adf9-abced8b0712a.jpg'),
            'proteins' => 11.9,
            'fats' => 8.3,
            'carbohydrates' => 49.6,
            'weight' => 1130,
            'category_id' => 2
        ]);

        Product::query()->create([
            'name' => 'Си Сяке',
            'price' => 1149,
            'image' => asset('image/products/f6540ace-8e7d-4baa-b29e-76e0902d0f02.jpg'),
            'proteins' => 11.9,
            'fats' => 8.3,
            'carbohydrates' => 49.6,
            'weight' => 1914,
            'category_id' => 2
        ]);

        Product::query()->create([
            'name' => 'Pepsi',
            'price' => 60,
            'image' => asset('image/products/8d45c4d6-5d7a-490c-8083-d63ef534e77d.jpg'),
            'proteins' => 0,
            'fats' => 0,
            'carbohydrates' => 74.6,
            'weight' => 500,
            'category_id' => 3
        ]);
        Product::query()->create([
            'name' => '7up',
            'price' => 60,
            'image' => asset('image/products/db44e719-70d2-43d0-b676-2214168f2f9b.jpg'),
            'proteins' => 0,
            'fats' => 0,
            'carbohydrates' => 0.6,
            'weight' => 500,
            'category_id' => 3
        ]);

    }
}
