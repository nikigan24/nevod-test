<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Checkout extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $address;
    public $phone;
    public $items;
    public $total;
    public $delivery;
    public $payment;
    public $text;

    /**
     * Checkout constructor.
     * @param $name
     * @param $address
     * @param $phone
     * @param $items
     * @param $total
     * @param $delivery
     * @param $payment
     */
    public function __construct($name, $address, $phone, $items, $total, $delivery, $payment, $text)
    {
        $this->name = $name;
        $this->address = $address;
        $this->phone = $phone;
        $this->items = $items;
        $this->total = $total;
        $this->delivery = $delivery;
        $this->payment = $payment;
        $this->text = $text;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.checkout');
    }
}
