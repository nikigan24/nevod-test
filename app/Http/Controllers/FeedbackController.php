<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Mail\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function feedback(FeedbackRequest $request)
    {
        $validated = $request->validated();

        Mail::to('gankin.n@yandex.ru')->send(new Feedback($validated['name'], $validated['email'], $validated['phone'], $validated['text']));

        return response()->json();
    }
}
