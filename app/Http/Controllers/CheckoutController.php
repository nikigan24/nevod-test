<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutRequest;
use App\Mail\Checkout;
use App\Models\OptionValue;
use App\Models\Product;
use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    public function checkout(CheckoutRequest $request)
    {

        $validated = $request->validated();

        $products = $request->get('products');
        $sum = 0;

        foreach ($products as $product) {
            $price = Product::query()->findOrFail($product['id'])->price;
            $option_price = OptionValue::query()->find($product['optionId'])->value ?? 0;

            $sum = $option_price ? $sum + $option_price : $sum + $price;
        }

        $delivery = $request->get('delivery');

        if ($delivery == 1) {
            $sum *= 0.9;
        }

        Mail::to('gankin.n@yandex.ru')->send(new Checkout($validated['name'], $validated['address'], $validated['phone'], $validated['items'], $sum, $validated['delivery'], $validated['payment'], $validated['text']));

        return response()->json();
    }
}
