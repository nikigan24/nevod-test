<?php

use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::apiResource('posts', PostController::class);

    Route::apiResource('products', ProductController::class);
    Route::get('products_with_categories', [ProductController::class, 'productsWithCategories']);

    Route::get('products/option/{id}', [ProductController::class, 'option']);

    Route::post('checkout', [CheckoutController::class, 'checkout']);

    Route::post('/feedback', [FeedbackController::class, 'feedback']);

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
