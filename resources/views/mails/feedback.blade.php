<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Обратная связь</title>
</head>
<body>
    <div>Имя: {{ $name }}</div>
    <div>Почта: {{ $email }}</div>
    <div>Телефон: {{ $phone }}</div>
    <p>
    <div>Текст:</div>
    {{ $text }}</p>
</body>
</html>
