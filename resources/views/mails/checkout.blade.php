<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Заказ</title>
</head>
<body>
    <div>Имя: {{ $name }}</div>
    <div>Адрес: {{ $address }}</div>
    <div>Телефон: {{ $phone }}</div>
    <div>Товары:</div>
    @foreach($items as $item)
        {{ $item['name'] }} {{$item['optionName']}} {{ $item['quantity'] }} x {{ $item['price'] }}
        = {{ $item['quantity'] *  $item['price']}}
    @endforeach
    <div>Доставка: {{ !$delivery ? 'Доставка' : 'Самовывоз' }}</div>
    <div>Оплата: {{ !$payment ? 'Наличными' : 'По карте' }}</div>
    <div>
        Комментарий:
        <p>{{ $text }}</p>
    </div>
    <div>Итого: {{ $total }}</div>
</body>
</html>
