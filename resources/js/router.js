import VueRouter  from "vue-router";
import Home from "./Pages/Home";
import NewsPage from "./Pages/NewsPage";
import PostPage from "./Pages/PostPage";
import AboutUsPage from "./Pages/AboutUsPage";
import ContactPage from "./Pages/ContactPage";
import CartPage from "./Pages/CartPage";

const router  = new VueRouter({
    mode: "history",
    routes: [
        {
            path: '/posts',
            component: NewsPage,
            name: "posts.index"
        },
        {
            path: '/posts/:id',
            component: PostPage,
            name: "post.show"
        },
        {
            path: '/',
            component: Home,
            name: "home",
        },
        {
            path: '/about_us',
            component: AboutUsPage,
            name: 'about_us'
        },
        {
            path: '/contact',
            component: ContactPage,
            name: 'contact'
        },
        {
            path: '/cart',
            component: CartPage,
            name: 'cart'
        }
    ]
})

export default router;
