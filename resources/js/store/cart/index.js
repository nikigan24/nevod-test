const state = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : {
    items: [],
    total: 0
};

const getters = {
    cartProducts: (state) => {
        return state.items;
    },
    cartTotalCount: (state) => {
        return state.items.reduce((total, item) => total + item.quantity, 0)
    },
    cartTotalPrice: (state, getters) => {
        return getters.cartProducts.reduce((total, product) => {
            return total + product.price * product.quantity
        }, 0)
    }
}

const actions = {
    checkout({commit}) {
        commit('setCartItems', {items: []})
    },

    addProductToCart({state, commit}, {id, selectedOption: optionId}) {
        commit('setCheckoutStatus', null)
        const cartItem = state.items.find(item => item.id === id && item.optionId === optionId)
        if (!cartItem) {
            commit('pushProductToCart', {id, optionId})
        } else {
            commit('incrementItemQuantity', cartItem)
        }
    },

    deleteItemFromCart({state, commit}, {index}) {
        commit('deleteItem', { index });
    }
}

const mutations = {
    async pushProductToCart(state, {id, optionId}) {
        const {data: product} = await axios.get(`/api/products/${id}`);

        let option = null;

        if (optionId) {
            const {data} = await axios.get(`/api/products/option/${optionId}`).catch(e => console.log(e.message));
            option = data;
        }

        state.items.push({
            id,
            name: product.name,
            price: option?.value ?? product.price,
            optionId,
            optionName: option?.option.name ?? null,
            quantity: 1
        });

        localStorage.setItem('cart', JSON.stringify(state));
    },

    incrementItemQuantity(state, {id}) {
        const cartItem = state.items.find(item => item.id === id)
        cartItem.quantity++
        localStorage.setItem('cart', JSON.stringify(state));
    },

    deleteItem(state, {index}) {
        const items = state.items;
        items.splice(index, 1);
        state.items = [...items];
        localStorage.setItem('cart', JSON.stringify(state));
    },

    setCartItems(state, {items}) {
        state.items = items
        localStorage.setItem('cart', JSON.stringify(state));
    },

    setCheckoutStatus(state, status) {
        state.checkoutStatus = status
        localStorage.setItem('cart', JSON.stringify(state));
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
