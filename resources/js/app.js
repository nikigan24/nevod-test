require('./bootstrap');

import Vue from 'vue';
import VueRouter  from "vue-router";
import router from "./router";
import VueNotification from "vue-notification";
import store from './store';
import App from "./components/App";

Vue.use(VueRouter);
Vue.use(VueNotification);

Vue.component('App', require('./components/App.vue').default)

const app = new Vue({
    el: "#app",
    components: { App },
    store,
    router
})
